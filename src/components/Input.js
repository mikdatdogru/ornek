import React from "react";
import PropTypes from "prop-types";

const Input = (props) => {
  return (
    <input
      className={props.className}
      name={props.name}
      onChange={props.onChange}
      type="text"
      value={props.inputData}
    />
  );
};

Input.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
  inputData: PropTypes.string.isRequired,
};
Input.defaultProps = {
  value: "",
  className: "new-todo",
  type: "text",
  name: "inputName",
};

export default Input;
