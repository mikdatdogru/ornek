import React from "react";
import PropTypes from "prop-types";

const Title = (props) => {
  return <h1>{props.val}</h1>;
};

Title.propTypes = {
  val: PropTypes.string,
};

Title.defaultProps = {
  val: "todos",
};

export default Title;
