import React from "react";

const TodoItem = (props) => {
  return (
    <li>
      <div className="view">
        <input className="toggle" type="checkbox" />
        <label>{props.value}</label>
        <button className="destroy" />
      </div>
      <input onChange={() => {}} className="edit" value="Rule the web" />
    </li>
  );
};

TodoItem.propTypes = {};
TodoItem.defaultProps = {};

export default TodoItem;
