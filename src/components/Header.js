import React from "react";
import PropTypes from "prop-types";
import Title from "./Title";
import Input from "./Input";

const Header = (props) => {
  return (
    <div>
      <header className={props.className}>{props.children}</header>
    </div>
  );
};

Header.propTypes = {
  className: PropTypes.string,
};

Header.defaultProps = {
  className: "headerClass",
};

export default Header;
