import React, { Component } from "react";
import Input from "./Input";

class TodoInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      val: "",
    };
  }

  onChange = (e) => {
    this.setState({
      val: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.onAdd(this.state.val);

    this.setState({
      val: "",
    });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <Input
          className="new-todo"
          onChange={this.onChange}
          name="inputName"
          inputData={this.state.val}
        />
      </form>
    );
  }
}

TodoInput.propTypes = {};
TodoInput.defaultProps = {};

export default TodoInput;
