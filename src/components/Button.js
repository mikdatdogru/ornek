import React from "react";

const Button = (props) => {
  return (
    <button className="sendButton" disabled={props.disabled}>
      {props.text}
    </button>
  );
};

Button.propTypes = {};
Button.defaultProps = {};

export default Button;
