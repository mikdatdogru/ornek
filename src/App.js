import React from "react";
import Home from "./pages/Home";

const App = () => {
  return <Home />;
};

App.propTypes = {};
App.defaultProps = {};

export default App;
