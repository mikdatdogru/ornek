import React, { Component } from "react";
import "../assets/css/index.css";
import "../assets/css/base.css";
import Header from "../components/Header";
import Title from "../components/Title";
import Input from "../components/Input";
import TodoInput from "../components/TodoInput";
import TodoItem from "../components/TodoItem";

class home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
    };
  }

  onAdd = (data) => {
    const newData = [...this.state.todos];
    newData.push(data);
    this.setState({
      todos: newData,
    });
  };

  render() {
    return (
      <section className="todoapp">
        <Header>
          <Title />
          <TodoInput onAdd={this.onAdd} />
        </Header>
        {/*This section should be hidden by default and shown when there are todos*/}
        <section className="main">
          <input id="toggle-all" className="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>
          <ul className="todo-list">
            {/*These are here just to show the structure of the list items*/}
            {/*List items should get the class `editing` when editing and `completed` when marked as completed*/}
            {/*

            <li className="completed">
              <div className="view">
                <input className="toggle" type="checkbox" checked />
                <label>Taste JavaScript</label>
                <button className="destroy" />
              </div>
              <input className="edit" value="Create a TodoMVC template" />
            </li>

            */}

            {this.state.todos.map((item, index) => {
              return <TodoItem key={`todo1-${index}`} value={item} />;
            })}
          </ul>
        </section>
        {/*This footer should hidden by default and shown when there are todos*/}
        <footer className="footer">
          {/*This should be `0 items left` by default*/}
          <span className="todo-count">
            <strong>0</strong> item left
          </span>
          {/*Remove this if you don't implement routing*/}
          <ul className="filters">
            <li>
              <a className="selected" href="#/">
                All
              </a>
            </li>
            <li>
              <a href="#/active">Active</a>
            </li>
            <li>
              <a href="#/completed">Completed</a>
            </li>
          </ul>
          {/*Hidden if no completed items are left ↓*/}
          <button className="clear-completed">Clear completed</button>
        </footer>
        <pre>{JSON.stringify(this.state.todos, null, 2)}</pre>
      </section>
    );
  }
}

home.propTypes = {};
home.defaultProps = {};

export default home;
